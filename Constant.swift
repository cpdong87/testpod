//
//  Constant.swift
//  Regen
//
//  Created by DONGCP on 12/5/16.
//  Copyright © 2016 DONGCP. All rights reserved.
//

import UIKit


class Constant: NSObject {
  
    static let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    static let shareAppDelegate = UIApplication.shared.delegate as! AppDelegate
    static let kOsVersion = (UIDevice.current.systemVersion as NSString ).floatValue
    
    static let documentDirectory = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
    static let tempAssetPath = Constant.documentDirectory + "/TempAsset"
    static let mainScrennSize = UIScreen.main.bounds.size
    
    
    struct appInfo {
        static let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        static let build = Bundle.main.infoDictionary?["CFBundleVersion"] as! String
        static let appName = Bundle.main.infoDictionary?["CFBundleName"] as! String
    }
  
   
    struct Platform {
        static let isSimulator: Bool = {
            var isSim = false
            #if arch(i386) || arch(x86_64)
                isSim = true
            #endif
            return isSim
        }()
    }
  
}
